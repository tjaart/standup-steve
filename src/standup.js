const moment = require('moment')

module.exports = {
  secondsToNextStandup: function (date, standupDays, standupTime) {
    let day = date.day()
    let time = date.format('HH:mm')

    let standupDate = moment(`${date.format('YYYY-MM-DD')} ${standupTime}`, ['YYYY-MM-DD HH:mm'])
    let matched = false
    while (!matched) {
      // Today is not a standup_day, skip to the next day
      if (standupDays.indexOf(day) < 0) {
        standupDate.add(1, 'days')
        day = (day + 1) % 7
        time = '00:00'
        continue
      } else {
        // Today is a standupDay, but the time has passed, skip to the next day
        if (time >= standupTime) {
          standupDate.add(1, 'days')
          day = (day + 1) % 7
          time = '00:00'
          continue
        }
      }
      matched = true
    }
    return parseInt((standupDate - date) / 1000)
  }
}
