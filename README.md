# Standup Steve #

An Open Source Standup Slack Bot, inspired by [Geekbot](https://geekbot.io/)

[![pipeline status](https://gitlab.com/tjaart/standup-steve/badges/master/pipeline.svg)](https://gitlab.com/tjaart/standup-steve/commits/master)
[![coverage report](https://gitlab.com/tjaart/standup-steve/badges/master/coverage.svg)](https://gitlab.com/tjaart/standup-steve/commits/master)
[![codebeat badge](https://codebeat.co/badges/1f8048d7-447c-4498-81c4-344931012b05)](https://codebeat.co/projects/gitlab-com-tjaart-standup-steve-master)

## Development sponsored by ##

[![Outside Open](assets/images/oo.png)](https://outsideopen.com)

## Is Standup Steve for you? ##

`Standup Steve` is not one of these "Bot as a service" deals. To get Steve on board, you have to get your hands dirty. 

If terms like "server hosting" or "API tokens" is Greek to you, let me recommend one of the available [commercial options](https://tjaart.slack.com/apps/search?q=standup).

Otherwise, if you choose the red pill, [read the docs](http://steve.tjaart.org/docs/)


## License ##

Standup Steve is published under the [MIT license](https://gitlab.com/tjaart/standup-steve/LICENCE)
