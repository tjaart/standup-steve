const moment = require('moment')
const standup = require('./standup')

describe('seconds to next standup', () => {
  it('should wait 1 minute', () => {
    expect(standup.secondsToNextStandup(
    // Monday morning 8:59... 60 seconds to standup
      moment('2018-07-09 08:59:00', 'YYYY-MM-DD HH:mm'),
      [1, 2, 3, 4, 5], '09:00')).toBe(60)
  })

  it('should wait 1 day - 60 seconds', () => {
    expect(standup.secondsToNextStandup(
    // Monday morning 9:01... 86340 seconds to standup
      moment('2018-07-09 09:01:00', 'YYYY-MM-DD HH:mm'),
      [1, 2, 3, 4, 5], '09:00')).toBe((24 * 60 * 60) - 60)
  })

  it('should wait 3 days - 60 seconds', () => {
    expect(standup.secondsToNextStandup(
    // Monday morning 9:01... 259140 seconds to standup
      moment('2018-07-13 09:01:00', 'YYYY-MM-DD HH:mm'),
      [1, 2, 3, 4, 5], '09:00')).toBe((24 * 60 * 60 * 3) - 60)
  })
})
