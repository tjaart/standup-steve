DIR=$(cd `dirname $0` && pwd)
REPO_ROOT=${DIR}/..
SITE_PATH=${DIR}/test
BUILD_PATH=${REPO_ROOT}/public
# If THEME_URL is a symlink, set this to true
THEME_SYMLINK=false
THEME_URL=https://github.com/tjaartvdwalt/gitlab-project-landing-page.git

hugo new site ${SITE_PATH}

cp ${DIR}/config.toml ${SITE_PATH}

if ${THEME_SYMLINK}; then
   ln -s -t ${SITE_PATH}/themes ${THEME_URL}
else
  cd ${SITE_PATH}/themes
  git clone ${THEME_URL}
  cd ${DIR}
fi

cp ${REPO_ROOT}/README.md ${SITE_PATH}/content

cd ${SITE_PATH}
hugo
cd ${DIR}

mkdir -p ${BUILD_PATH}
cp -r ${SITE_PATH}/public/* ${BUILD_PATH}

rm -rf ${SITE_PATH}
