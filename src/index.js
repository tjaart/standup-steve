const config = require('config')
const moment = require('moment')
const { RTMClient, WebClient } = require('@slack/client')
const standup = require('./standup')
const winston = require('winston')

const transports = {
  console: new winston.transports.Console({ level: config.get('log.level') }),
  file: new winston.transports.File({
    filename: config.get('log.file'),
    level: config.get('log.level')
  })
}

const log = winston.createLogger({transports: [transports.console, transports.file]})

const rtm = new RTMClient(config.get('slack.bot-token'))
const web = new WebClient(config.get('slack.bot-token'))

const debug = config.get('debug')
const standupDays = config.get('standup.days')
const standupTime = config.get('standup.time')

const yesterdayQuestion = 'What did you do yesterday?'
const todayQuestion = 'What do you intend to do today?'
const impedimentsQuestion = 'Do you have any impediments?'
const doneText = 'Happy Hacking!'

async function getChannel (channelName) {
  let channel = (await web.channels.list()).channels.find((element) => {
    return element.name === channelName
  })
  if (!channel) {
    throw new Error(`Channel '${channelName}' not found`)
  }

  let channelObj = await web.channels.info({'channel': channel.id})

  if (!channelObj) {
    throw new Error(`Channel '${channelName}' not found`)
  }

  return (await web.channels.info({'channel': channel.id})).channel
}

// read a message from the user, and return the text
function readMessage (user) {
  return new Promise(function promise (resolve, reject) {
    var func = new Function( // eslint-disable-line no-new-func
      'action', 'return function ' + user + '(event){ action(event) }')(
      function (event) {
        if (event.type === 'message' && event.user === user) {
          rtm.removeListener('message', this)
          resolve(event.text)
        }
      })

    rtm.on('message', func)
  })
}

// ask the standup questions
function standupQuestions (im, user, timeout) {
  return new Promise(async function promise (resolve, reject) {
    var done = false
    setTimeout(function () {
      if (!done) {
        rtm.listeners('message').forEach((listener) => {
          if (listener.name === user) {
            rtm.removeListener('message', listener)
          }
        })
        reject(Error(`Standup timed out after ${timeout} seconds.`))
      }
    }, timeout * 1000)

    await web.chat.postMessage({channel: im.channel.id, text: yesterdayQuestion})
    let yesterday = await readMessage(user, timeout)

    await web.chat.postMessage({channel: im.channel.id, text: todayQuestion})
    var today = await readMessage(user, timeout)

    await web.chat.postMessage({channel: im.channel.id, text: impedimentsQuestion})
    var impediments = await readMessage(user, timeout)

    await web.chat.postMessage({channel: im.channel.id, text: doneText})

    done = true
    resolve({yesterday, today, impediments})
  })
}

async function postResults (userId, channel, responses) {
  try {
    let username = (await web.users.info({user: userId})).user.name
    let standupText = `<@${username}> posted a status update:
*${yesterdayQuestion}*
${responses.yesterday.split('\n').map((el) => '>' + el).join('\n')}
*${todayQuestion}*
${responses.today.split('\n').map((el) => '>' + el).join('\n')}`

    if (!['no', 'none', 'nope', 'na', 'n/a'].includes(responses.impediments.toLowerCase())) {
      standupText += `
*${impedimentsQuestion}*
${responses.impediments.split('\n').map((el) => '>' + el).join('\n')}`
    }
    await web.chat.postMessage({channel: channel, text: standupText})
  } catch (error) {
    log.error(error)
  }
}

async function runStandup (channel, timeout = 3600) {
  let members = (await getChannel(channel)).members

  members.forEach(async function (user) {
    try {
      var im = await web.im.open({user})
      let responses = await standupQuestions(im, user, timeout)
      postResults(user, channel, responses)
      // web.im.close({channel: user})
    } catch (e) {
      web.chat.postMessage({channel: im.channel.id, text: `*Error:*\n${e.message}`})
    }
  })
}

// sleep for 10 seconds
function sleep (seconds) {
  return new Promise(function promise (resolve, reject) {
    setTimeout(function () {
      resolve()
    }, seconds * 1000)
  })
}

async function main () {
  const channel = config.get('slack.channel')
  rtm.start()

  // keep our bot going to infinity
  while (true) {
    const timeout = debug ? 30 : config.get('standup.timeout')
    const nextStandup = debug ? 60 : standup.secondsToNextStandup(
      moment(), standupDays, standupTime)
    try {
      web.chat.postMessage({channel: channel,
        attachments: [{
          'color': 'good',
          'title': 'Time for a standup!',
          'text': `Standup started at ${moment()}, and will timeout after ${timeout / 60} minutes`
        }]
      })
      runStandup(channel, timeout)
    } catch (e) {
      log.error(e)
    }
    log.debug(`Next standup in: ${nextStandup} seconds`)
    await sleep(nextStandup)
  }
}

try {
  main()
} catch (e) {
  log.error(e)
}
